import '../../domain/domain.dart';

class ProductsRepositoryImpl extends ProductsRepository {
  final ProductsDatasource productsDatasource;

  ProductsRepositoryImpl(this.productsDatasource);

  @override
  Future<Product> createUpdateProduct(Map<String, dynamic> productLike) {
    return productsDatasource.createUpdateProduct(productLike);
  }

  @override
  Future<Product> getProductsById(String id) {
    return productsDatasource.getProductsById(id);
  }

  @override
  Future<List<Product>> getProductsByPage({int limit = 10, int offset = 0}) {
    return productsDatasource.getProductsByPage(limit: limit, offset: offset);
  }

  @override
  Future<List<Product>> searchProductByTerm(String term) {
    return productsDatasource.searchProductByTerm(term);
  }
}
